#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>

#include "TLorentzVector.h"

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
}



StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK (book (TH1F ("h_HPt", "h_HPt", 100, 0, 500))); // H pt [GeV]
  ANA_CHECK (book (TH1F ("h_HEta", "h_HEta", 100, -5, 5))); // H eta
  ANA_CHECK (book (TH1F ("h_mjj", "h_mjj", 100, 0, 2500))); // H eta

  m_eventCount = 0;

  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  // print out run and event number from retrieved object
  if(m_eventCount % 1000 == 0) {
    ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
  }
  m_eventCount++;

  // loop over the truth particles in the container
  const xAOD::TruthParticleContainer* truthParts = nullptr;
  ANA_CHECK (evtStore()->retrieve (truthParts, "TruthBoson"));

  bool foundH = false;

  TLorentzVector H(0,0,0,0);

  for (const xAOD::TruthParticle* p : *truthParts) {
    // H must have two children
    if (p->child(0) != nullptr && p->child(1) != nullptr){
      // find the H
      if ((p->pdgId() == 25) && !foundH){
        foundH = true;
        H.SetPtEtaPhiM(p->pt(),p->eta(),p->phi(),p->m());
      }
    }
  } // end for loop over truth parts


  float mcEventWeight = eventInfo->mcEventWeight();
  hist ("h_HPt")->Fill (H.Pt() * 0.001, mcEventWeight); // GeV
  hist ("h_HEta")->Fill (H.Eta() , mcEventWeight); 

  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}
